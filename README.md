> Migrated to monorepo https://bitbucket.org/mvfglobal/rhino-monorepo/src/master/packages/servicer-express/

# MVF Servicer Express

## Usage

### To install the package

Run `npm install @mvf/servicer-express`

### Configuration
Set the following environment variables in your project

- `ENVIRONMENT_FILE` should be set to `development` | `testing` | `staging` | `production`
- `EXPRESS_SERVER_PORT`

### Express Web

This version of the express integration implements basic express server preset and has no limitations in how the server can be used. By default the preset implements:
* logging
* error handler

To setup this server you will need an `entrypoint.ts` or equivalent file.

```ts
import { Application, DaemonCommand } from '@mvf/servicer';
import { ExpressWeb } from '@mvf/servicer-express';
import { Express, json } from 'express';

const setup = (app: Express) => {
  app.use(json());

  // do whatever you want with the express server setup
}

const setupApplication = async (): Promise<void> => {
  const application = new Application();
  application.addCommands(
    new DaemonCommand(new ExpressWeb(setup),
  );
  await application.run();
};

void setupApplication();
```

When the above is compiled, the server can be started with `node entrypoint.js express-web-daemon` command.

### Express Api

This is a version of express implementation designed for internal `api services` and optimized for `vertical slice architecture`. Configuration of this implementation is deliberately constrained.

__Input Constraints__

Only json inputs are valid.

__Output Constraints__

Only json output is valid.

Output will always have the following structure for successful responses, except in the case of status 204 where the output will be empty.
```json
{
  "data": { ... },
}
```
At the moment output will always have the following structure for error responses. This will likely change in the future due to the fact that it is not good enough for a lot of cases.
```json
{
  "error": "error message",
}
```

To setup this server you will need an `entrypoint.ts` or equivalent file. In addition, you will also need to setup [EventSources](./docs/expressEventSource.md) and [routes](./docs/routes.md), these are needed for the `ExpressApi` constructor.

```ts
import { Application, DaemonCommand } from '@mvf/servicer';
import { ExpressApi } from '@mvf/servicer-express';
import { routes } from 'Setup/Express';
import { EventSources } from 'EventSources/Express';

const setupApplication = async (): Promise<void> => {
  const application = new Application();
  application.addCommands(
    new DaemonCommand(new ExpressApi(EventSources, routes),
  );
  await application.run();
};

void setupApplication();
```

When the above is compiled, the server can be started with `node entrypoint.js express-api-daemon` command.

## Contributing

### Setup

- Run `make` to build the container
- Run `make shell` to enter the container
- Run `npm install` to install dependencies

Refer to package.json for commands

### After merging

After you have merged a PR to master, you need to rebuild and publish your changes.

1. Checkout master `git checkout master && git pull`
2. Use one of the following make publish commands to publish changes:
   - `make publish kind=patch` - Use this if your change is a bug fix and is backwards compatible.
   - `make publish kind=minor` - Use this if your change adds new functionality and is backwards compatible.
   - `make publish kind=major` - Use this if your change is not backwards compatible.
