import { each } from 'lodash';
import { IEventSource } from '@mvf/servicer';
import { json } from 'express';
import ExpressWeb from './ExpressWeb';
import IRoute from './IRoute';
import IExpressConfig from './IExpressConfig';
import { IExpressHeaders } from './IExpressHeaders';

class ExpressApi extends ExpressWeb {
  static readonly PORT = process.env.EXPRESS_SERVER_PORT
    ? process.env.EXPRESS_SERVER_PORT
    : 80;

  static readonly TYPE = 'express';

  constructor(
    source: IEventSource<any, IExpressHeaders>,
    routes: IRoute[],
    config?: IExpressConfig,
  ) {
    super((app) => {
      app.use(json());
      each(routes, (route: IRoute): void => {
        const preconditions =
          config && config.preconditions ? config.preconditions : [];
        app[route.method](route.path, route.action(source, preconditions));
      });
    }, config);
  }

  getName = (): string => `${ExpressWeb.TYPE}-api`;
}

export default ExpressApi;
