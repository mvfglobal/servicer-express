/* eslint-disable consistent-return */
import { error } from '@mvf/logger';
import { ErrorRequestHandler } from 'express-serve-static-core';

const errorHandler: ErrorRequestHandler = (
  err: Error & { status?: number },
  _request,
  response,
  next,
) => {
  if (response.headersSent) {
    return next(err);
  }

  response.status(err.status ?? 500);

  if (process.env.ENVIRONMENT_FILE === 'development') {
    response.json({ error: err.message });
  } else {
    response.json({ error: 'An unexpected error occurred.' });
    error({ message: err.message });
  }
};

export default errorHandler;
