import { IPayload } from '@mvf/servicer';

export interface IExpressHeaders<
  TParams extends IPayload = IPayload,
  TQuery extends IPayload = IPayload
> extends IPayload {
  params: TParams;
  query: TQuery;
}
