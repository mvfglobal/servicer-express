import { logger } from '@mvf/logger/express';
import express, { Express as ExpressServer } from 'express';
import cookieParser from 'cookie-parser';
import { info } from '@mvf/logger';
import { IDaemon } from './IDaemon';
import IExpressConfig from './IExpressConfig';
import errorHandler from './errorHandler';

class ExpressWeb implements IDaemon {
  static readonly PORT = process.env.EXPRESS_SERVER_PORT
    ? process.env.EXPRESS_SERVER_PORT
    : 80;

  static readonly TYPE = 'express';

  public readonly app: ExpressServer;

  constructor(setup: (app: ExpressServer) => void, config?: IExpressConfig) {
    this.app = express();
    this.app.use(cookieParser());
    this.app.use(logger());
    this.app.get('/healthcheck', (_req, res) => {
      res.status(200).send('Okay!');
    })

    setup(this.app);

    this.app.use(
      config && config.errorHandler ? config.errorHandler : errorHandler,
    );
  }

  getName = (): string => `${ExpressWeb.TYPE}-web`;

  start = (): Promise<void> => {
    this.app.listen(ExpressWeb.PORT, this.started);
    return new Promise(() => {});
  };

  private started = (): void => {
    const location = `Location 'localhost:${ExpressWeb.PORT}'`;
    const environmentFile = process.env.ENVIRONMENT_FILE ?? 'undefined';
    const environment = `Environment '${environmentFile}'`;
    info(`Express server ready | ${location} | ${environment}`);
  };
}

export default ExpressWeb;
