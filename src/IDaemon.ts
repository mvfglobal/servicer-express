export interface IDaemon {
  start(): Promise<void>;
  getName(): string;
}
