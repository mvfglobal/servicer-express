import { ErrorRequestHandler, Dictionary } from 'express-serve-static-core';
import { IPayload, Effect } from '@mvf/servicer';
import { IExpressHeaders } from './IExpressHeaders';
import { ExpressAction } from './ExpressAction';

interface IExpressConfig {
  preconditions?: ExpressAction<IPayload, IExpressHeaders, Effect.ITypes>[];
  errorHandler?: ErrorRequestHandler<Dictionary<string>>;
}

export default IExpressConfig;
