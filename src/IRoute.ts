import IActionRoute from './IActionRoute';

export default interface IRoute {
  method: 'get' | 'post' | 'put' | 'delete';
  path: string;
  action: IActionRoute;
}
