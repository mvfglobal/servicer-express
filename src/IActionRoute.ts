import { Dictionary, RequestHandler } from 'express-serve-static-core';
import { IEventSource, IPayload, Effect } from '@mvf/servicer';
import { ExpressAction } from './ExpressAction';
import { IExpressHeaders } from './IExpressHeaders';

type IActionRoute = (
  source: IEventSource<any, IExpressHeaders>,
  preconditions: ExpressAction<IPayload, IExpressHeaders, Effect.ITypes>[],
) => RequestHandler<Dictionary<string>>;

export default IActionRoute;
