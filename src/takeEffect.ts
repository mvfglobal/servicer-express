/* eslint-disable consistent-return */
import { Response } from 'express';
import snakeCaseKeys from 'snakecase-keys';
import { Effect, IPayload } from '@mvf/servicer';

export interface IOutput {
  response: (data: IPayload | string) => void;
  data: IPayload | string;
}

const takeEffect = (
  response: Response,
  effect: Effect.ITypes,
): IOutput | void => {
  if (!effect) {
    return;
  }

  switch (effect.type) {
    case Effect.Type.OUTPUT: {
      const data = { data: effect.payload.output };
      return {
        response: response.json.bind(response),
        data: snakeCaseKeys(data, { deep: true }),
      };
    }
    case Effect.Type.STATUS: {
      response.status(effect.payload.status);
      break;
    }
    case Effect.Type.NOTHING: {
      response.status(204);
      return {
        response: response.send.bind(response),
        data: '',
      };
    }
    case Effect.Type.RAW: {
      const contentTypes = {
        css: 'text/css',
        js: 'application/js',
      };

      response.header('content-type', contentTypes[effect.payload.type]);
      return {
        response: response.send.bind(response),
        data: effect.payload.output,
      };
    }
    default: {
      break;
    }
  }
};

export default takeEffect;
