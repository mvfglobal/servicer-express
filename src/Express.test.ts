import ExpressWeb from './ExpressWeb';
import ExpressApi from './ExpressApi';

describe('Express', () => {
  it("should return 'express-api' when getName is called", () => {
    expect(new ExpressApi({}, [], { preconditions: [] }).getName()).toEqual(
      'express-api',
    );
  });

  it("should return 'express-web' when getName is called", () => {
    expect(new ExpressWeb(() => {}).getName()).toEqual('express-web');
  });
});
