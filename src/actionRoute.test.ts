import * as Logger from '@mvf/logger/client';
import { Effect, next } from '@mvf/servicer';
import actionRoute from './actionRoute';

const makeAsyncGenerator = (...effects: Effect.ITypes[]) => {
  return async function* () {
    yield* effects;
  };
};

const makeMiddlewareAsyncGenerator = (
  triggerNext: boolean,
  ...effects: Effect.ITypes[]
) => {
  return async function* (payload: unknown) {
    yield* effects;
    if (triggerNext) {
      yield next(payload as any);
    }
  };
};

describe('actionRoute.ts', () => {
  const spyLog = jest.spyOn(Logger, 'error');
  const eventName = 'EVENT_A';
  const invalidEventName = 'EVENT_B';
  const precondition = makeMiddlewareAsyncGenerator(true);
  const request: any = { params: {}, query: {}, body: { test: 'data' } };
  const response: any = {};

  afterEach(() => {
    spyLog.mockClear();
  });

  it('should pass request and response into the action', async () => {
    const spyAction = jest.fn().mockImplementation(makeAsyncGenerator());
    const source = { EVENT_A: spyAction };
    await actionRoute(eventName)(source, [precondition])(
      request,
      response,
      () => {},
    );
    expect(spyAction).toHaveBeenCalledWith({
      headers: { params: {}, query: {} },
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      body: (request.body as unknown) ?? {},
    });
  });

  it('should log error if the event is invalid', async () => {
    const source = {
      EVENT_A: jest.fn().mockImplementation(makeAsyncGenerator()),
    };
    await actionRoute(invalidEventName)(source, [precondition])(
      request,
      response,
      () => {},
    );
    expect(spyLog).toHaveBeenNthCalledWith(
      1,
      `Invalid event '${invalidEventName}' provided, valid event list: ["${eventName}"]`,
    );
  });

  it('should log error if precondition throws an error', async () => {
    const source = {
      EVENT_A: jest.fn().mockImplementation(makeAsyncGenerator()),
    };
    const preconditionError = async function* () {
      throw new Error('Precondition error');
    };

    await actionRoute(eventName)(source, [preconditionError])(
      request,
      response,
      () => {},
    );
    expect(spyLog).toHaveBeenNthCalledWith(1, 'Precondition error');
  });

  it('should log error if action throws an error', async () => {
    const source = {
      EVENT_A: jest.fn().mockImplementation(async function* () {
        throw new Error('Action error');
      }),
    };

    await actionRoute(eventName)(source, [precondition])(
      request,
      response,
      () => {},
    );
    expect(spyLog).toHaveBeenNthCalledWith(1, 'Action error');
  });
});
