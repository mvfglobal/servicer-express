import { Action, IPayload, Effect } from '@mvf/servicer';
import { IExpressHeaders } from './IExpressHeaders';

export type ExpressAction<
  TBody extends IPayload = IPayload,
  THeaders extends IExpressHeaders = IExpressHeaders,
  TEffects extends Effect.ITypes = void
> = Action<TBody, THeaders, TEffects>;
