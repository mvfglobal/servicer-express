import camelcaseKeys from 'camelcase-keys';
import { error } from '@mvf/logger';
import { IPayload as IBody, middleware } from '@mvf/servicer';
import IActionRoute from './IActionRoute';
import { IExpressHeaders } from './IExpressHeaders';
import takeEffect, { IOutput } from './takeEffect';
import InvalidEventError from './InvalidEventError';

interface IPayload {
  headers: IExpressHeaders;
  body: IBody;
}

const actionRoute = (eventName: string): IActionRoute => (
  source,
  preconditions,
) => async (request, response, next): Promise<void> => {
  try {
    const action = source[eventName];
    if (!action) {
      throw new InvalidEventError(eventName, source);
    }

    const payload: IPayload = {
      headers: {
        params: camelcaseKeys(request.params),
        query: camelcaseKeys(request.query),
      },
      body: (camelcaseKeys(request.body, {
        deep: true,
      }) as unknown) as IPayload,
    };

    const generator = middleware(...preconditions)(action)(payload);
    let output: IOutput | void;
    // eslint-disable-next-line no-restricted-syntax
    for await (const nextYield of generator) {
      const result = takeEffect(response, nextYield);
      if (result) {
        output = result;
      }
    }

    if (!output) {
      throw new Error('Action did not yield output');
    }

    if (output) {
      output.response(output.data);
    }
  } catch (err) {
    if (err instanceof Error) {
      error(err.message);
    }

    next(err);
  }
};

export default actionRoute;
