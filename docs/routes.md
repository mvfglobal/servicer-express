# Routes
Routes is a simple array of object where each object is a route description, these will define your express routes. When a route is triggered an event is publish, this event then triggers an [action](https://bitbucket.org/mvfglobal/servicer/src/master/docs/actions.md).

A route has 3 properties that have to be provided:

* `method` of the route
* `path` of the route
* `action` that will be triggered when the route is called.

The only thing to note here is that for the `action` property you should use `actionRoute` helper function, this function takes `event` you defined in your [EventSources](./expressEventSource.md) as input.

```ts
import { actionRoute, Method, IRoute } from '@mvf/servicer-express';
import { ExpressEvent } from 'EventSources/Express';

const routes: IRoute[] = [
  {
    method: Method.POST,
    path: '/v1/measurements',
    action: actionRoute(ExpressEvent.SAVE_MEASUREMENTS_REQUESTED),
  },
];
```

## Recommendations

* `Setup/Express/routes.ts` is the recommended location for this file.
