# Express Api Action Headers
For the `express` event source `action headers` include the following properties:

* __params__ - if you have defined any parameters in the url then those parameters can be found here, e.g. `/books/:bookId`, bookId param could be found under `payload.headers.params.bookId`.
* __query__ - if there were query parameters provided with the request then those parameters can be found here, e.g. `/someUrl?name=Bob`, name query could be found under `payload.headers.query.name`. 

When defining an `ActionType` you can use `IExpressHeaders` helper type to enforce this structure, e.g.

```ts
import { Effect, Action, IPayload, IExpressHeaders } from '@mvf/servicer';

type ActionType = Action<IPayload, IExpressHeaders, Effect.INothing>;
```

If you would like to define the exact structure of `params` and `query` then you can provide two optional type parameters for `IExpressHeaders<..., ...>`

* __Input 1__: Defines the type of parameters.
* __Input 2__: Defines the type of query.

so for example, you could do

```ts
import { Effect, Action, IPayload, IExpressHeaders } from '@mvf/servicer';

interface IParams {
  bookId: string;
}

interface IQuery {
  name: string;
}

type Headers = IExpressHeaders<IParams, IQuery>;
type ActionType = Action<IPayload, IExpressHeaders, Effect.INothing>;
```

If you only want to specify the query type then you can use `IPayload` type for the params, e.g.

```ts
type Headers = IExpressHeaders<IPayload, IQuery>;
```
