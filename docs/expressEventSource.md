# Express Event Source

Read the event sources [introduction](https://bitbucket.org/mvfglobal/servicer/src/master/docs/EventSources.md) first.

To create an express event source follow this pattern.

first, create `EventSources/Express/ExpressEvents.ts` file and populate it with the following stub.

```ts
import { IEventSource, bindSimulator, IPayload } from '@mvf/servicer';
import { IExpressHeaders } from '@mvf/servicer-express';
import { postMeasurementsAction } from 'Actions/PostMeasurements';

export enum ExpressEvent {
  SAVE_MEASUREMENTS_REQUESTED = 'SAVE_MEASUREMENTS_REQUESTED',
}

const ExpressEvents: IEventSource<typeof ExpressEvent, IExpressHeaders, IPayload> = {
  [ExpressEvent.SAVE_MEASUREMENTS_REQUESTED]: postMeasurementsAction,
};

export const simulate = bindSimulator(ExpressEvents);

export default ExpressEvents;
```

* `ExpressEvent` lists valid `events`.
* `ExpressEvents` object associates `events` to [actions](https://bitbucket.org/mvfglobal/servicer/src/master/docs/actions.md).
* [simulate](https://bitbucket.org/mvfglobal/servicer/src/master/docs/simulate.md) function is used to simplify the execution of the action which is useful in tests, or if you want to trigger one action from another action.

then, define your event types and map them to desired actions, remove example type and mapping.

finally, create `EventSources/Express/index.ts`

```ts
export {
  default as ExpressEvents,
  ExpressEvent,
  simulate,
} from './ExpressEvents';
```

## Recommendations

* Use standard naming conventions for events, an event name should always describe what has happened, e.g. `POST_MEASUREMENTS` is a bad event name, this event name does not describe what has happened. `SAVE_MEASUREMENTS_REQUESTED` is a good event name, because it describes what has happened.
