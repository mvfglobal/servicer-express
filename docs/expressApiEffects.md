# Express Api Effect Behaviors

## Output
Response status will be set to `200`, then json response will be returned to the caller.

## Nothing
Response status will be set to `204`, then request will complete.

## Next
undefined behavior, nothing will happen.
